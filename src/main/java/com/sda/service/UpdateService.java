package com.sda.service;

import com.sda.model.Consult;
import com.sda.model.Pet;
import com.sda.model.Veterinarian;

public class UpdateService extends GenericDBService {
    public static void update(int option) {
        switch (option) {
            case 1:
                Consult consult = consultDao.findID();
                if(consult != null){
                    Consult consult1 = CreateService.createConsult();
                    assert consult1 != null;
                    consult1.setId(consult.getId());
                    consultDao.update(consult1);
                }
                break;
            case 2:
                Pet pet = petDao.findID();
                if(pet != null){
                  Pet petToUpdate = CreateService.createPet();
                  petToUpdate.setId(pet.getId());
                  petDao.update(petToUpdate);
                }
                break;
            case 3:
                Veterinarian veterinarian = veterinarianDao.findID();
                if(veterinarian !=null){
                    Veterinarian veterinarianToUpdate = CreateService.createVeterinarian();
                    veterinarianToUpdate.setId(veterinarian.getId());
                    veterinarianDao.update(veterinarianToUpdate);
                }
                break;
        }
    }

}
