package com.sda.service;

import com.sda.model.*;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class CreateService extends GenericDBService {

    /**
     * I chose a switch as it simplifies the verification of an option.
     * At the same time if someone chose the first option which is a consult and there are no veterinarians this app
     * will ask the user to add a veterinarian first.
     * @param option of the user
     */
    public static void create(int option) {
        switch (option) {
            case 1:
                Consult consult = createConsult();
                if(consult == null){
                    System.out.println("There are no veterinarians for this consult. Please add one first.");
                    create(3);
                    consult = createConsult();
                }
                consultDao.save(consult);
                break;
            case 2:
                Pet pet1 = createPet();
                petDao.addInDB(pet1);
                break;
            case 3:
                Veterinarian veterinarian = createVeterinarian();
                veterinarianDao.addInDB(veterinarian);
                break;
        }
    }

    /**
     * This method is required as a member for the Consult object as we will also need the time of the consult not
     * only the date.
     * @return java.sql.Timestamp
     */
    public static Timestamp convertStringToTimestamp() {
        System.out.println("1. Consult date (format dd/MM/yyyy HH:mm:ss): ");
        String time = in.nextLine();
        Date date;
        try {
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            date = formatter.parse(time);
            if(time.equals(formatter.format(date))){
                date = new Timestamp(date.getTime());
            }
        } catch (ParseException e) {
            System.out.println("Exception :" + e);
            return null;
        }
        if(date == null){
            date = convertStringToTimestamp();
        }
        return (Timestamp) date;
    }

    /**
     *  Prepares a Pet object to be added on the DB.
     *  I chose a regex to give to the @isVaccinated variable true or false based on yes/Yes/y/y
     *  Any of those 4 strings will return true.
     * @return Pet
     */
    public static Pet createPet() {
        System.out.println("Race of your pet: ");
        String race = in.nextLine();
        System.out.println("Is your pet vaccinated? y/n : ");
        Boolean isVaccinated = in.hasNext("^([y|Y]es|[y|Y])");
        System.out.println("Please provide the owner name: ");
        String ownerName = in.nextLine();
        System.out.println("Please provide the birth date of your Pet: ");
        java.sql.Date birth_date = createDate();
        return new Pet(race, birth_date, isVaccinated, ownerName);
    }

    /**
     *  This method prepares a Consult to be added on the database. Now the Pet object which is required for the
     *  consult will have either be in the database or ,if it's a new one, added. As for the Veterinarian object, i
     *  chose to assign one using random.
     * @return Consult
     */
    public static Consult createConsult() {
        System.out.println("Please supply the following: \n");
        Long numberOfVets = (Long) veterinarianDao.count();
        Timestamp timestamp = convertStringToTimestamp();
        System.out.println("2. Description of the consult: ");
        String description = in.nextLine();
        System.out.println("Please provide the pet you need to consult: ");
        Pet pet = createPet();
        pet = petDao.addInDB(pet);
        System.out.println();
        if(numberOfVets == 0){
            return null;
        }
        return new Consult(timestamp, description,
                veterinarianDao.findById((long)(Math.random() * 1) + numberOfVets), pet);
    }

    /**
     * Method to prepare an Veterinarian object to be added on the DB.
     * @return Veterinarian
     */
    public static Veterinarian createVeterinarian() {
        System.out.println("First name of the veterinarian: ");
        String firstName = in.nextLine();
        System.out.println("Last name of the veterinarian: ");
        String lastName = in.nextLine();
        System.out.println("Address of the veterinarian: ");
        String address = in.nextLine();
        System.out.println("Speciality of the veterinarian: ");
        String speciality = in.nextLine();
        return new Veterinarian(firstName, lastName, address, speciality);
    }

    /**
     * This method is for creating a Date object using a String which will be created based on a format.
     * If by any chance the format is wrong, it will ask again for the input.
     * @return Date
     */
    public static java.sql.Date createDate(){
        System.out.println("Please follow the following date format dd/MM/yyyy :");
        System.out.println("year: ");
        String year = in.nextLine();
        System.out.println("month: ");
        String month = in.nextLine();
        System.out.println("day: ");
        String day = in.nextLine();
        java.sql.Date birth_date = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            birth_date = (java.sql.Date) formatter.parse(day + "/" + month + "/" + year);
            if(!(day + "/" + month + "/" + year).equals(formatter.format(birth_date))){
                birth_date = null;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(birth_date == null){
            birth_date = createDate();
        }
        return birth_date;
    }
}
