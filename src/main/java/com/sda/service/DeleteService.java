package com.sda.service;

import com.sda.model.Consult;
import com.sda.model.Pet;
import com.sda.model.Veterinarian;

public class DeleteService extends GenericDBService{

    public static void delete(int option) {
        switch (option) {
            case 1:
                Consult consult = consultDao.findID();
                if(consult != null){
                    consultDao.delete(consult);
                }
                break;
            case 2:
                Pet pet = petDao.findID();
                if(pet != null){
                    petDao.delete(pet);
                }
                break;
            case 3:
                Veterinarian veterinarian = veterinarianDao.findID();
                if(veterinarian !=null){
                    veterinarianDao.delete(veterinarian);
                }
                break;
        }
    }
}
