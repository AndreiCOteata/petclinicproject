package com.sda.service;

import com.sda.dao.ConsultDao;
import com.sda.dao.PetDao;
import com.sda.dao.VeterinarianDao;
import lombok.Getter;

import java.util.Scanner;

/**
 * I'm using this class so I won't have to use new Dao objects in other classes.
 * I think this isn't the best call, but what the hell.
 */
@Getter
public class GenericDBService {
    static final Scanner in = new Scanner(System.in);

    static final PetDao petDao = new PetDao();
    static final ConsultDao consultDao = new ConsultDao();
    static final VeterinarianDao veterinarianDao = new VeterinarianDao();
}
