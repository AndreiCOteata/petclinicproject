package com.sda.service;

public class DisplayService {
    static void optionsMenu(){
        System.out.println("Please choose one of the following options:\n" +
                "0. Exit\n" +
                "1. Create\n" +
                "2. Update\n" +
                "3. Delete\n" +
                "4. List/Find\n");
    }
    static void modelMenu(){
        System.out.println("Please choose the entity you wish to do the operation:\n" +
                "1. Consult\n" +
                "2. Pet\n" +
                "3. Veterinarian");
    }
}
