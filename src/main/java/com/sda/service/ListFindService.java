package com.sda.service;

import com.sda.model.*;

public class ListFindService extends GenericDBService {

    /**
     * I wanted to keep this method as simple as possible.
     * The methods below will try to find in the Database the entry with the specified Id and if it fails it will
     * list them.
     * I've could've generalized this one as it's against open-closed principle from SOLID.
     * @param option of the user
     */
    public static void update(int option) {
        switch (option) {
            case 1:
                Consult consult = consultDao.findID();
                if(consult != null){
                    System.out.println(consult);
                }
                break;
            case 2:
                Pet pet = petDao.findID();
                if(pet != null){
                    System.out.println(pet);
                }
                break;
            case 3:
                Veterinarian veterinarian = veterinarianDao.findID();
                if(veterinarian!=null){
                    System.out.println(veterinarian);
                }
                break;
        }
    }
}
