package com.sda.service;

import java.util.Scanner;

public class Application {

    static Scanner in = new Scanner(System.in);

    public static void startApp() {
        do {
            DisplayService.optionsMenu();
            System.out.println("Your input:");
            int userInput = in.nextInt();
            if (userInput < 0 || userInput > 4) {
                System.out.println("Invalid option, please try again!");
                continue;
            }
            if (userInput == 0) {
                System.out.println("Goodbye!");
                break;
            }
            DisplayService.modelMenu();
            int modelOption;
            do {
                System.out.println("Please insert your option: ");
                modelOption = in.nextInt();
                if (modelOption < 1) {
                    System.out.println("Invalid option, please try again!");
                    continue;
                }
                modelOptionFunction(modelOption, userInput);
            } while (modelOption > 4);
        } while (true);
    }
    public static void modelOptionFunction(int modelOption, int userInput){
        switch (userInput){
            case 1:
                CreateService.create(modelOption);
                break;
            case 2:
                UpdateService.update(modelOption);
                break;
            case 3:
                DeleteService.delete(modelOption);
                break;
            case 4:
                ListFindService.update(modelOption);
                break;
        }
    }








}
