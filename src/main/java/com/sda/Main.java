package com.sda;

import com.sda.service.Application;

public class Main {
    public static void main(String[] args) {
        Application.startApp();
    }
}
