package com.sda.dao;

import com.sda.dao.generic.GenericDaoImpl;
import com.sda.model.Consult;

import java.util.Scanner;

public class ConsultDao extends GenericDaoImpl<Consult> {

    public ConsultDao() {
        super(Consult.class);
    }

}
