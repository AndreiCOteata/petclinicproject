package com.sda.dao;

import com.sda.dao.generic.GenericDaoImpl;
import com.sda.model.Pet;

public class PetDao extends GenericDaoImpl<Pet> {
    public PetDao() {
        super(Pet.class);
    }
}
