package com.sda.dao;

import com.sda.dao.generic.GenericDaoImpl;
import com.sda.model.Veterinarian;

public class VeterinarianDao extends GenericDaoImpl<Veterinarian> {
    public VeterinarianDao() {
        super(Veterinarian.class);
    }
}
