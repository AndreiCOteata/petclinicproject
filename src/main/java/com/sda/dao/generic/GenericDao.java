package com.sda.dao.generic;


import java.io.Serializable;
import java.util.List;

public interface GenericDao<T extends Serializable> {
    void save(T obj);
    void update(T obj);
    void delete(T obj);
    T findById(Long id);
    List<T> findAll();
}
