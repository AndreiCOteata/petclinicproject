package com.sda.dao.generic;

import com.sda.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.Serializable;
import java.util.List;
import java.util.Scanner;

public class GenericDaoImpl<T extends Serializable> implements GenericDao<T> {
    private final Class<T> clazz;

    static final Scanner in = new Scanner(System.in);

    protected GenericDaoImpl(Class<T> clazz) {
        this.clazz = clazz;
    }

    private static Session session;

    @Override
    public void save(T obj) {
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(obj);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }finally {
            if(session != null){
                session.close();
            }
        }
    }

    @Override
    public void update(T obj) {
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.update(obj);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }finally {
            if(session != null){
                session.close();
            }
        }
    }

    @Override
    public void delete(T obj) {
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.delete(obj);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
        }finally {
            if(session != null){
                session.close();
            }
        }
    }

    /**
     * Search in the database for an entry with a specific Id.
     * If there's no entry with that id, the return will be null and it will list all the entries.
     * Now if the list is null it will output a message saying so.
     * I could've some custom exceptions for the null return but this implementations looks good as it is.
     * @param id - primary key of the object in the DB
     * @return - object if found if now, it will return null.
     */
    @Override
    public T findById(Long id) {
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            T type = session.find(clazz, id);
            if(type == null){
                List<T> tList = this.findAll();
                if(tList == null){
                    System.out.println("There are no entries in the database at all!");
                }else{
                    displayAll(tList);
                    System.out.println("As you can see there's no entry in the database with that id.");
                }
                return null;
            }else{
                return type;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }finally {
            if(session != null){
                session.close();
            }
        }
    }

    @Override
    public List<T> findAll(){
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            return session.createQuery("from " + clazz.getName(), clazz).list();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally {
            if(session != null){
                session.close();
            }
        }
    }

    public Number count(){
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            return (Number) session.createQuery("select count(*) from " + clazz.getName())
                    .uniqueResult();
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }finally {
            if(session != null){
                session.close();
            }
        }
    }

    public T addInDB(T obj) {
        List<T> petList = this.findAll();
        int index = 0;
        for (T each : petList) {
            if (each.equals(petList.get(index))) {
                return petList.get(index);
            }
            index++;
        }
        this.save(obj);
        obj = addInDB(obj);
        return obj;
    }

    public void displayAll(List<T> list){
        for (T each: list) {
            System.out.println(each);
        }
    }
    public T findID(){
        System.out.println("Please provide the id: ");
        long option = in.nextInt();
        return this.findById(option);
    }
}
