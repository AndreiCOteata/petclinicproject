package com.sda.model;


import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Setter
@Getter
@NoArgsConstructor
@ToString
public class Consult implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    private java.sql.Timestamp consultDate;
    private String description;

    @OneToOne(mappedBy = "consult")
    private Pet pet;

    @ManyToOne()
    @JoinColumn(name="veterinarian_id")
    private Veterinarian veterinarian;


    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    @ToString.Exclude
    private Date createDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_date")
    @ToString.Exclude
    private Date updateDate;

    public Consult(java.sql.Timestamp consult_date, String description, Veterinarian veterinarian, Pet pet){
        this.consultDate = consult_date;
        this.description = description;
        this.veterinarian = veterinarian;
        this.pet = pet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Consult consult = (Consult) o;
        return getConsultDate().equals(consult.getConsultDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getConsultDate());
    }
}
