package com.sda.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Setter
@Getter
@NoArgsConstructor
@ToString
public class Pet implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    private String race;
    private java.sql.Date birth_Date;
    private Boolean isVaccinated;
    private String ownerName;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "consult_id")
    private Consult consult;



    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    @ToString.Exclude
    private Date createDate;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_date")
    @ToString.Exclude
    private Date updateDate;

    public Pet(String race, java.sql.Date birth_Date, Boolean isVaccinated, String ownerName){
        this.race = race;
        this.birth_Date = birth_Date;
        this.isVaccinated = isVaccinated;
        this.ownerName = ownerName;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return getRace().equals(pet.getRace()) &&
                getBirth_Date().equals(pet.getBirth_Date()) &&
                getIsVaccinated().equals(pet.getIsVaccinated()) &&
                getOwnerName().equals(pet.getOwnerName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRace(), getBirth_Date(), getIsVaccinated(), getOwnerName());
    }
}
